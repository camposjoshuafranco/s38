

//DOMSelectors

//getElementById
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');

//getElementByClass
const inputFields = document.getElementsByClassName('form-control')

console.log(firstName);
console.log(lastName);

console.log(inputFields);